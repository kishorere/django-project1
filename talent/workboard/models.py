from django.db import models

# Create your models here.

class Work(models.Model):
    title = models.CharField(max_length=100)
    text = models.TextField()
    contact_number = models.CharField(max_length=20)

    def __str__(self):
        return self.title,self.text,self.contact_number
