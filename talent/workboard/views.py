

from django.shortcuts import render, redirect
from .models import Work

def work_board(request):
    if request.method == 'POST':
        title = request.POST['title']
        text = request.POST['text']
        contact_number = request.POST['contact_number']
        notice = Work(title=title, text=text, contact_number=contact_number)
        notice.save()
        return redirect('work_board')
    return render(request, 'notice_list.html', {'works': Work})
