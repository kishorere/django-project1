from django.urls import path
from .import views 

urlpatterns = [
    path('work_board/all', views.work_board, name='work_board'),
]